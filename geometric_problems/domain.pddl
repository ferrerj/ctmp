;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Robot domain with mobile objects
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; A robot moves around a 2D room with possible obstacles.
;;; The encoding uses state constraints to handle the non-overlapping requirement
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain fn-robot-navigation)

    (:types
        ;; The integer type
        int - object

        ;; Any mobile body
        body_id - object

        ;; A mobile body with autonomous motion
        robot_id - body_id

        ;; A mobile body without autonomous motion - only these can be moved by the robots
        object_id - body_id

        ;; an identifier of a configuration
        conf_id - object

        ;; a possible direction for a translation
        tdirection - object

        ;; a possible direction for a rotation
        rdirection - object
    )

    (:constants
        clockwise counterclockwise - rdirection
        N NE E SE S SW W NW - tdirection
    )

    (:predicates
        (holding ?o - object_id)
        (handempty ?r - robot_id)
        (graspable ?r - robot_id ?r_conf - conf_id ?b - body_id ?b_conf)

        ;; Returns the configuration that results from performing the given operation, or -1 otherwise
        (@translate       ?b - body_id ?from - conf_id ?d - tdirection)
        (@rotate          ?b - body_id ?from - conf_id ?d - rdirection)
        (@rotate_with_obj ?b - body_id ?from - conf_id ?d - rdirection)


    )

    (:functions
        ;; Returns the current configuration of the given body
        (conf ?b - body_id) - conf_id

        ;; Returns the configuration that results from translating in the given direction body ?b when in configuration ?c.
        ;; Might return a null configuration if the translation is not possible
        (translation ?b - body_id ?c - conf_id ?d - tdirection) - conf_id

        ;; Returns the configuration that results from rotating in the given direction body ?b when in configuration ?c.
        ;; Might return a null configuration if the rotation is not possible
        (rotation ?b - body_id ?c - conf_id ?d - rdirection) - conf_id

        (translate_obj_robot_rot ?b - body_id ?c - conf_id ?d - rdirection) - conf_id

        ;; True iff the given configuration is the goal configuration for the given robot --- static predicate.
        (goal_conf ?r - robot_id) - conf_id

    )

    ;; robot translation
    (:action translate_robot :parameters (?r - robot_id ?d - tdirection)
        :precondition (and (handempty ?r))
        :effect (and (assign (conf ?r) (@translate ?r (conf ?r) ?d)) )
    )

    ;; robot rotation
    (:action rotate_robot :parameters (?r - robot_id ?d - rdirection)
        :precondition (and (handempty ?r) )
        :effect (and (assign (conf ?r) (@rotate ?r (conf ?r) ?d)) )
    )

    (:action grasp-object
      :parameters (?r - robot_id ?o - object_id)
      :precondition (and 
        (handempty ?r)   
        (graspable ?r (conf ?r) ?o (conf ?o))
        )
      :effect (and 
            (holding ?o)
            (not(handempty ?r))
        )
      )


    (:action place-object
        :parameters (?r - robot_id ?o - object_id)
        :precondition (and (holding ?o) )
        :effect (and
            (handempty ?r)
            (not(holding ?o))
            )
    )

    ;; Object translation - robot ?r translates object ?o
    (:action translate_object :parameters (?r - robot_id ?o - object_id ?d - tdirection)
        :precondition
            (and (holding ?o))
        :effect
            (and
                (assign (conf ?o) (@translate ?o (conf ?o) ?d))
                (assign (conf ?r) (@translate ?r (conf ?r) ?d))

            )
    )

    (:action rotate_robot_obj :parameters (?r - robot_id ?o - object_id ?d - rdirection)
        :precondition
          (and (holding ?o))
            :effect(and
                (assign (conf ?r) (@rotate ?r (conf ?r) ?d))
                (assign (conf ?o) (@rotate_with_obj ?o (conf ?o) ?d))
                )
        )

)
